FROM centos:latest
MAINTAINER ingktds <tadashi.1027@gmail.com>

ENV VERSION "redis-3.2.0"
RUN yum -y install gcc make
RUN curl -LO http://download.redis.io/releases/${VERSION}.tar.gz
RUN tar xzf ${VERSION}.tar.gz
RUN cd $VERSION && make
RUN cd $VERSION && make install
ADD redis.conf /etc/redis.conf

EXPOSE 6379
CMD [ "/usr/local/bin/redis-server", "/etc/redis.conf" ]
